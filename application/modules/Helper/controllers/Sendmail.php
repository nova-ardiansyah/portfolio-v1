<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sendmail extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('sendmail');
  }

  public function report_inbound_message()
  {
    $sendmail = sendcustom_email([
      'emailTo' => $_ENV['EMAIL_RECEIVER'],
      'type'    => 'report_inbound_message',
      'data' => [
        'uid'        => uuid(),
        'email'      => $_ENV['EMAIL_RECEIVER'],
        'subject'    => 'Testing Inbound Message',
        'message'    => 'Testing Inbound Message',
        'name'       => $_ENV['AUTHOR_FULLNAME'],
        'created_at' => getTimestamp(),
        'from_url'   => base_url()
      ]
    ]);

    json($sendmail);
  }

  public function reply_inbound_message()
  {
    $sendmail = sendcustom_email([
      'emailTo' => $_ENV['EMAIL_RECEIVER'],
      'type'    => 'reply_inbound_message',
      'data' => [
        'name' => $_ENV['AUTHOR_FULLNAME']
      ]
    ]);

    json($sendmail);
  }
}
