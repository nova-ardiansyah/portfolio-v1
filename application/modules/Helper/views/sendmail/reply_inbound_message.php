<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Thank You for Reaching Out</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #040b14;
      color: #fefefe;
      margin: 0;
      padding: 0;
    }

    .container {
      max-width: 600px;
      margin: 0 auto;
      padding: 20px;
      background-color: #2c2f3f;
      border-radius: 10px;
      box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
    }

    h3 {
      color: #fefefe;
    }

    p {
      margin-bottom: 20px;
      line-height: 1.6;
      color: #fefefe;
    }

    a {
      color: #ffcd38 !important;
      opacity: 0.8;
      text-decoration: none;
    }

    a:hover {
      color: #ffcd38;
      opacity: 1;
    }
  </style>
</head>

<body>
  <div class="container">
    <h3>Hello <?= $name; ?>!</h3>
    <p>Thank you for reaching out to me. I appreciate your message and I'm committed to responding as promptly as possible, typically within 24 hours.</p>
    <p>If you have any further questions or would like to discuss more details, please feel free to get in touch with me again.</p>
    <p>Once again, thank you for your interest in collaborating. I look forward to providing solutions that meet your needs.</p>
    <p>You can also reach out to me directly via <a href="https://wa.me/6289506668480?text=Hi%20Nova%2C%20I%20would%20like%20to%20connect%20with%20you%20soon!" target="_blank">WhatsApp</a> for a more immediate response.</p>
    <p>Kind regards,</p>
    <p><?= $_ENV['AUTHOR_FULLNAME']; ?></p>
  </div>
</body>

</html>