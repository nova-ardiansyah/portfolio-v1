<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>New Message Notification</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #040b14;
      color: #fefefe;
      margin: 0;
      padding: 0;
    }

    .container {
      max-width: 600px;
      margin: 0 auto;
      padding: 20px;
      background-color: #2c2f3f;
      border-radius: 10px;
      box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
    }

    h3 {
      color: #fefefe;
    }

    p {
      margin-bottom: 20px;
      line-height: 1.6;
      color: #fefefe;
    }

    a {
      color: #ffcd38 !important;
      opacity: 0.8;
      text-decoration: none;
    }

    a:hover {
      color: #ffcd38;
      opacity: 1;
    }

    .message-details {
      background-color: #212431;
      color: #ffffff;
      padding: 10px;
      border-radius: 5px;
    }

    .message-details b {
      color: #fff;
    }
  </style>
</head>

<body>
  <div class="container">
    <h3>New Message Notification</h3>
    <p>You have received a new message from your website. Here are the details:</p>
    <div class="message-details">
      <p>
        <b>Name:</b> <?= $name; ?><br>
        <b>Email:</b> <?= $email; ?><br>
        <b>Subject:</b> <?= $subject; ?><br>
        <b>Message:</b> <?= $message; ?><br>
        <b>Ticket:</b> <?= $uid; ?><br>
        <b>Send at:</b> <?= $created_at; ?><br>
        <b>Send from:</b> <?= $from_url; ?><br>
      </p>
    </div>
    <p>Thank you for your attention!</p>
    <p>Kind regards,</p>
    <p><?= $_ENV['AUTHOR_FULLNAME']; ?></p>
  </div>
</body>

</html>
