<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />

  <title><?= $_ENV['AUTHOR_FULLNAME']; ?></title>
  <meta content="Passionate Web and Mobile Developer | Problem Solver" name="description" />
  <meta content="Web Developer, Mobile Developer, Problem Solver, Technology Enthusiast" name="keywords" />

  <!-- Favicons -->
  <link href="<?= base_url('assets/img/favicon.png'); ?>" rel="icon" />
  <link href="<?= base_url('assets/img/apple-touch-icon.png'); ?>" rel="apple-touch-icon" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

  <!-- Vendor CSS Files -->
  <link href="<?= base_url('assets/vendor/aos/aos.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/vendor/bootstrap-icons/bootstrap-icons.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/vendor/glightbox/css/glightbox.min.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/vendor/swiper/swiper-bundle.min.css'); ?>" rel="stylesheet" />

  <!-- Template Main CSS File -->
  <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <!-- ======= Header ======= -->
  <?php $this->load->view('Main/section/main/header'); ?>
  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container" data-aos="fade-in">
      <h1><?= $_ENV['AUTHOR_FULLNAME']; ?></h1>
      <p>I'm <span class="typed" data-typed-items="Web Developer, Mobile Developer, Student"></span></p>
    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= About Section ======= -->
    <?php $this->load->view('Main/section/main/about'); ?>
    <!-- End About Section -->

    <!-- ======= Facts Section ======= -->
    <?php // $this->load->view('Main/section/main/facts'); ?>
    <!-- End Facts Section -->
    
    <!-- ======= Skills Section ======= -->
    <?php $this->load->view('Main/section/main/skills'); ?>
    <!-- End Skills Section -->
    
    <!-- ======= Resume Section ======= -->
    <?php $this->load->view('Main/section/main/resume'); ?>
    <!-- End Resume Section -->

    <!-- ======= Portfolio Section ======= -->
    <?php $this->load->view('Main/section/main/portfolio'); ?>
    <!-- End Portfolio Section -->

    <!-- ======= Services Section ======= -->
    <?php // $this->load->view('Main/section/main/services'); ?>
    <!-- End Services Section -->
    
    <!-- ======= Testimonials Section ======= -->
    <?php // $this->load->view('Main/section/main/testimonials'); ?>
    <!-- End Testimonials Section -->
    
    <!-- ======= Contact Section ======= -->
    <?php $this->load->view('Main/section/main/contact'); ?>
    <!-- End Contact Section -->
    
  </main><!-- End #main -->
  
  <!-- ======= Footer ======= -->
  <?php $this->load->view('Main/section/main/footer'); ?>
  <!-- End  Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center">
    <i class="bi bi-arrow-up-short"></i>
  </a>

  <!-- Vendor JS Files -->
  <script src="<?= base_url('assets/vendor/purecounter/purecounter_vanilla.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/aos/aos.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/glightbox/js/glightbox.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/isotope-layout/isotope.pkgd.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/swiper/swiper-bundle.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/typed.js/typed.umd.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/waypoints/noframework.waypoints.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/php-email-form/validate.js'); ?>"></script>

  <!-- Important -->
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js" crossorigin="anonymous"></script>
  <script>
    const conf = {
      base_url: (path = '') => {
        return '<?= base_url(); ?>' + path
      },
      csrf: '<?= $this->security->get_csrf_hash(); ?>',
      csrf_name: '<?= $this->security->get_csrf_token_name(); ?>'
    }

    $(document).ready(function() {
      $('input, textarea, select, checkbox, radio').on('change click focus blur', function() {
        const propertyName = $(this).attr('name')
        $(`.invalid-feedback.${propertyName}`).html('').hide()
      })
    })
  </script>

  <?php if (isset($script)) : ?>
    <?php foreach ($script as $path) : ?>
      <script src="<?= $path; ?>" crossorigin="anonymous"></script>
    <?php endforeach; ?>
  <?php endif; ?>
</body>

</html>