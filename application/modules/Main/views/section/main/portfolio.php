<section id="portfolio" class="portfolio section-bg pb-0">
  <div class="container">
    <div class="section-title">
      <h2>Gallery</h2>
      <p>Discover a visual journey of my innovative projects that blend art and technology. Each piece is a testament to my passion for crafting unique and impactful experiences. Join me on this exciting ride through a world where imagination knows no limits.</p>
    </div>

    <div class="row" data-aos="fade-up">
      <div class="col-lg-12 d-flex justify-content-center">
        <ul id="portfolio-flters">
          <li data-filter="*" class="filter-active">All</li>
          <li data-filter=".filter-metaverse">Metaverse</li>
          <li data-filter=".filter-3D">3D Model</li>
          <!-- <li data-filter=".filter-web">Web</li> -->
        </ul>
      </div>
    </div>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="100">
      <div class="col-lg-4 col-md-6 portfolio-item filter-3D">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/threejs-ai-product/image-1.png'); ?>" class="img-fluid" alt="image-1.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/ai-threejs-product" title="More Details" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-metaverse">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/metaverse/image-2.png'); ?>" class="img-fluid" alt="image-2.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/metaverse-modern-webapp" title="Metaverse" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-3D">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/threejs-ai-product/image-2.png'); ?>" class="img-fluid" alt="image-2.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/ai-threejs-product" title="More Details" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-metaverse">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/metaverse/image-3.png'); ?>" class="img-fluid" alt="image-3.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/metaverse-modern-webapp" title="Metaverse" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-3D">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/threejs-ai-product/image-3.png'); ?>" class="img-fluid" alt="image-3.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/ai-threejs-product" title="More Details" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-metaverse">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/metaverse/image-1.png'); ?>" class="img-fluid" alt="image-2.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/metaverse-modern-webapp" title="Metaverse" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-3D">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/threejs-ai-product/image-4.png'); ?>" class="img-fluid" alt="image-4.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/ai-threejs-product" title="More Details" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->

      <div class="col-lg-4 col-md-6 portfolio-item filter-metaverse">
        <div class="portfolio-wrap">
          <img src="<?= base_url('assets/img/portfolio/metaverse/image-4.png'); ?>" class="img-fluid" alt="image-3.png" />
          <div class="portfolio-links">
            <a href="https://github.com/novaardiansyah/metaverse-modern-webapp" title="Metaverse" style="width: 100%;" target="_blank">
              <i class="bx bx-link"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /.portfolio-item -->
    </div>
  </div>
</section>