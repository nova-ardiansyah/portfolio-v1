<header id="header">
  <div class="d-flex flex-column">

    <div class="profile">
      <img src="<?= base_url('assets/img/profile-img.jpg') ?>" alt="image-profile" class="img-fluid rounded-circle" />
      <h1 class="text-light"><a href="<?= base_url(); ?>"><?= $_ENV['AUTHOR_FULLNAME']; ?></a></h1>
      <div class="social-links mt-3 text-center">
        <a href="https://www.facebook.com/Nova981" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="https://www.instagram.com/novaardiansyah._/" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="https://www.linkedin.com/in/novaardiansyah/" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        <a href="https://github.com/novaardiansyah" class="github"><i class="bx bxl-github"></i></a>
      </div>
    </div>

    <nav id="navbar" class="nav-menu navbar">
      <ul class="pb-5 mb-4">
        <li><a href="#hero" class="nav-link scrollto active"><i class="bx bx-home"></i> <span>Home</span></a></li>
        <li><a href="#about" class="nav-link scrollto"><i class="bx bx-user"></i> <span>About</span></a></li>
        <li><a href="#resume" class="nav-link scrollto"><i class="bx bx-file-blank"></i> <span>Resume</span></a></li>
        <li><a href="#portfolio" class="nav-link scrollto"><i class="bx bx-book-content"></i> <span>Gallery</span></a></li>
        <li class="d-none"><a href="#services" class="nav-link scrollto"><i class="bx bx-server"></i> <span>Services</span></a></li>
        <li><a href="#contact" class="nav-link scrollto"><i class="bx bx-envelope"></i> <span>Contact</span></a></li>
      </ul>
    </nav><!-- .nav-menu -->
  </div>
</header>