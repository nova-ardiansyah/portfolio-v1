<section id="resume" class="resume pb-0">
  <div class="container">

    <div class="section-title">
      <h2>Resume</h2>
      <p>Welcome to my professional journey—a path where passion meets purpose and innovation knows no bounds. With each step I've taken, I've embraced challenges and grown as a developer. My story is one of dedication, creativity, and a relentless pursuit of excellence. As you delve into my resume, you'll discover the experiences and skills that have shaped me into the developer I am today. Let's explore how I can bring value and expertise to your projects and initiatives.</p>
    </div>

    <div class="row">
      <div class="col-lg-6" data-aos="fade-up">
        <h3 class="resume-title">Sumary</h3>
        <div class="resume-item pb-0">
          <h4><?= $_ENV['AUTHOR_FULLNAME']; ?></h4>
          <p><em>Passionate and deadline-driven Web and Mobile Developer with 2+ years of experience crafting and delivering user-focused web and mobile applications from initial ideation to polished, functional products.</em></p>
          <ul>
            <li>Tangerang Selatan, Banten, Indonesia</li>
            <li>(+62) 895 0666 8480</li>
            <li>novaardiansyah.site</li>
          </ul>
        </div>

        <h3 class="resume-title">Education</h3>
        <div class="resume-item">
          <h4>Bachelor of Science in Computer Science</h4>
          <h5>2021 - present</h5>
          <p><em>Universitas Pamulang, Tangerang Selatan, Indonesia</em></p>
        </div>
        <div class="resume-item">
          <h4>Diploma in Software Engineering</h4>
          <h5>2017 - 2019</h5>
          <p><em>SMK Negeri 1 Trimurjo, Lampung Tengah, Indonesia.</em></p>
        </div>
      </div>
      <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
        <h3 class="resume-title">Professional Experience</h3>
        <div class="resume-item">
          <h4>Junior Web and Mobile Development</h4>
          <h5>2021 - Present</h5>
          <p><em>SofwareHouse, Tangerang, Indonesia.</em></p>
          <ul>
            <li>Play a pivotal role in identifying and resolving complex backend and frontend challenges for clients, ensuring seamless application functionality and user satisfaction.</li>
            <li>Take charge of application orchestration, overseeing its smooth operation for both clients and customers within the company.</li>
            <li>Collaborate closely with cross-functional teams, directing the implementation of solutions and ensuring efficient problem resolution</li>
            <li>Serve as a key liaison between clients and internal teams, facilitating effective communication and alignment of project goals.</li>
            <li>Streamline application workflows and processes, enhancing overall user experiences and driving customer satisfaction.</li>
          </ul>
        </div>
      </div>
    </div>

  </div>
</section>