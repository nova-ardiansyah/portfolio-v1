<section id="contact" class="contact">
  <div class="container">

    <div class="section-title">
      <h2>Contact</h2>
      <p>Feel free to reach out to me if you have any inquiries or if you're interested in collaborating on projects. I'm just an email or phone call away! Let's connect and explore the possibilities of working together to bring innovative ideas to life.</p>
    </div>

    <div class="row" data-aos="fade-in">

      <div class="col-lg-5 d-flex align-items-stretch">
        <div class="info">
          <div class="address">
            <i class="bi bi-geo-alt"></i>
            <h4>Location:</h4>
            <p>Tangerang Selatan, Banten, Indonesia.</p>
          </div>

          <div class="email">
            <i class="bi bi-envelope"></i>
            <h4>Email:</h4>
            <p>novaardiansyah78@gmail.com</p>
          </div>

          <div class="phone">
            <i class="bi bi-phone"></i>
            <h4>Phone:</h4>
            <p>(+62) 895 0666 8480</p>
          </div>

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.1402103273913!2d106.64711587326333!3d-6.2452467611483105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fbe305fd83c3%3A0x2b95e8d6d18aca25!2sBundaran%20Alam%20Sutera!5e0!3m2!1sid!2sid!4v1691053130612!5m2!1sid!2sid" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
        </div>

      </div>

      <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
        <form action="" method="post" role="form" class="php-email-form">
          <div class="row">
            <div class="form-group col-md-6">
              <label for="name">Your Name</label>
              <input type="text" name="name" class="form-control" id="name" required>
              <div class="invalid-feedback name"></div>
            </div>
            <div class="form-group col-md-6">
              <label for="email">Your Email</label>
              <input type="email" class="form-control" name="email" id="email" required>
              <div class="invalid-feedback email"></div>
            </div>
          </div>

          <div class="form-group">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" name="subject" id="subject" required>
            <div class="invalid-feedback subject"></div>
          </div>

          <div class="form-group">
            <label for="message">Message</label>
            <textarea class="form-control" name="message" rows="10" required></textarea>
            <div class="invalid-feedback message"></div>
          </div>

          <div class="my-3">
            <div class="loading">Loading</div>
            <div class="error-message"></div>
            <div class="sent-message">Your message has been sent. Thank you!</div>
          </div>

          <span id="alert-cant-contact"></span>

          <div class="text-center">
            <button type="submit" onclick="return send_message(event)">Send Message</button>
          </div>
        </form>
      </div>

    </div>

  </div>
</section>