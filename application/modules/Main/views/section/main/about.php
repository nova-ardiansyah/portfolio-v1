<section id="about" class="about">
  <div class="container">

    <div class="section-title">
      <h2>About</h2>
      <p class="mb-2">Hi there, I'm <?= $_ENV['AUTHOR_FIRSTNAME'] ?>!</p>
      <p>I'm a passionate web developer based in Tangerang Selatan, Banten, Indonesia. With a background in software engineering and a love for crafting clean and efficient code, I'm dedicated to creating engaging and user-friendly digital experiences.</p>
      <p class="d-none">As a current active student pursuing a Bachelor's degree in Computer Science at Universitas Pamulang, I'm constantly driven by the thrill of learning and exploring new technologies. My journey in the world of programming started during my high school years, where I specialized in Software Engineering. This foundation has since led me to become a versatile developer, proficient in both web and mobile development.</p>
    </div>

    <div class="row">
      <div class="col-lg-4" data-aos="fade-right">
        <img src="<?= base_url('assets/img/profile-img.jpg'); ?>" class="img-fluid" alt="profile-img" />
      </div>
      <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
        <h3>Web Developer &amp; Mobile Developer.</h3>
        <p class="fst-italic">
          Let's make something great together!
        </p>
        <div class="row">
          <div class="col-lg-6">
            <ul>
              <li><i class="bi bi-chevron-right"></i> <strong>Birthday:</strong> <span>11 November 2001</span></li>
              <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>novaardiansyah.site</span></li>
              <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>(+62) 895 0666 8480</span></li>
              <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>Tangerang, Indonesia</span></li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul>
              <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span id="age" data-birthdate="2001-11-11">-</span></li>
              <li class="d-none"><i class="bi bi-chevron-right"></i> <strong>Degree:</strong> <span>-</span></li>
              <li><i class="bi bi-chevron-right"></i> <strong>Status:</strong> <span>Active Student</span></li>
              <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span><?= $_ENV['EMAIL_RECEIVER']; ?></span></li>
              <li><i class="bi bi-chevron-right"></i> <strong>Freelance:</strong> <span>Not Available</span></li>
            </ul>
          </div>
        </div>
        <p>
          As a current active student pursuing a Bachelor's degree in Computer Science at Universitas Pamulang, I'm constantly driven by the thrill of learning and exploring new technologies. My journey in the world of programming started during my high school years, where I specialized in Software Engineering. This foundation has since led me to become a versatile developer, proficient in both web and mobile development.
        </p>
      </div>
    </div>

  </div>
</section>