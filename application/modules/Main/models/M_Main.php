<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Main extends CI_Model 
{
  public function __construct()
  {
    parent::__construct();
  }

  public function save_log()
  {
    $visited_key = 'visitor-log';
      
    if ($this->session->userdata($visited_key)) return FALSE;

    $this->session->set_userdata($visited_key, true, 15 * 60); // * 15 Minutes
    $this->load->library('user_agent');

    $data = [
      'uid'        => uuid(),
      'ip_address' => $this->input->ip_address(),
      'user_agent' => $this->agent->agent_string(),
      'platform'   => $this->agent->platform(),
      'browser'    => $this->agent->browser(),
      'version'    => $this->agent->version(),
      'referrer'   => $this->agent->referrer(),
      'description'=> 'User visit from ' . $this->agent->platform() . ' using ' . $this->agent->browser() . ' ' . $this->agent->version(),
      'created_at' => getTimestamp()
    ];

    $this->db->insert('logs', $data);
    $result = $this->db->affected_rows();

    return $result > 0 ? TRUE : FALSE;
  }

  public function list_inbound_messages()
  {
    $result = $this->db->query("SELECT * FROM inbound_messages")->result();
    return $result;
  }

  public function send_inbound_message()
  {
    $this->load->helper('sendmail');
    $limit_time = 5; // * Hour [0 for no limit]
    
    $email = post('email');
    if ($limit_time != 0)
    {
      $visited_key = 'send_inbound_message';
      
      if (!$this->session->userdata($visited_key)) {
        $this->session->set_userdata($visited_key, true, $limit_time * 3600);
      } else {
        return [
          'status'  => FALSE,
          'message' => "You have already sent a message. Please wait for our response. Thank you!"
        ];
      }
      
      $last_message = $this->db->query("SELECT a.id, a.uid FROM inbound_messages AS a WHERE a.email = ? AND created_at >= DATE_SUB(NOW(), INTERVAL ? HOUR)", [$email, $limit_time])->row();
  
      if ($last_message) {
        return [
          'status'  => FALSE,
          'message' => "You have already sent a message. Please wait for our response. Thank you!"
        ];
      }
    }

    $send = [
      'uid'        => uuid(),
      'name'       => post('name'),
      'email'      => $email,
      'subject'    => post('subject'),
      'message'    => post('message'),
      'created_at' => getTimestamp()
		];

    $this->db->insert('inbound_messages', $send);
    $result = $this->db->affected_rows();

    $sendmail = sendcustom_email([
      'emailTo' => 'novaardiansyah78@gmail.com',
      'type'    => 'report_inbound_message',
      'data'    => array_merge($send, [
        'from_url' => base_url()
      ])
    ]);

    $sendmail = sendcustom_email([
      'emailTo' => $email,
      'type'    => 'reply_inbound_message',
      'data'    => array_merge($send, [
        'from_url' => base_url()
      ])
    ]);

    return [
      'status'  => $result > 0 ? TRUE : FALSE,
      'message' => $result > 0 ? "We have received your message. We'll be in touch with you shortly. Thank you!" : "Oops! It seems there was an issue processing your request. Please try again later or contact us through alternative means. We apologize for any inconvenience this may have caused.",
      'data'    => $send
    ];
  }
}