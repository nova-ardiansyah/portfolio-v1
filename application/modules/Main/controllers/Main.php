<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MX_Controller  
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Main', 'Main');
	}
	
	public function index()
	{
		$this->Main->save_log();

		$data = [
			'script' => [
				base_url('assets/js/main/index.js'),
				base_url('assets/js/main/inbound_messages.js')
			]
		];

		$this->load->view('main', $data);
	}

	public function send_inbound_message()
	{
		$validate = $this->_send_inbound_message();
		
		if ($validate->run() == false) {
			return json([
				'status'  => false,
				'message' => 'invalid-form',
				'error'   => $validate->error_array()
			]);
		}

		$result = $this->Main->send_inbound_message();
		return json($result);
	}

	private function _send_inbound_message()
	{
		$rules = [
			['field' => 'name', 'label' => 'Your Name', 'rules' => 'trim|required'],
			['field' => 'email', 'label' => 'Your Email', 'rules' => 'trim|required|valid_email'],
			['field' => 'subject', 'label' => 'Subject', 'rules' => 'trim|required'],
			['field' => 'message', 'label' => 'Message', 'rules' => 'trim|required|min_length[10]']
		];

		return $this->form_validation->set_rules($rules);
	}
}
