console.log('inbound_messages.js loaded')

function send_message(event)
{
  event.preventDefault()
  console.log('send_message() called')
  
  let form     = $(event.target).closest('form')
  let formData = new FormData(form[0])
  let url      = conf.base_url('Main/send-inbound-message')
  
  formData.append(conf.csrf_name, conf.csrf)

  $.ajax({
    url,
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    beforeSend: function () {
      $(event.target).prop('disabled', true).hide()
      $(form).find('.loading').fadeIn(150)
      $(form).find(`.invalid-feedback`).html('').hide()
    },
    success: function(response) {
      conf.csrf = response.csrf
      
      $(form).find('.loading').fadeOut(150)
      
      if (!response.status) {
        if (response.message == 'invalid-form') {
          $(event.target).prop('disabled', false).show()

          Object.keys(response.error).forEach(function (key) {
            let error = response.error[key]
            $(form).find(`.invalid-feedback.${key}`).html(error).show()
          })
  
          return false
        }

        $(form).find('.sent-message').html('').hide()
        $(form).find('.error-message').html(response.message).fadeIn(150)
        return false
      }
      
      form[0].reset()
      $(form).find('.error-message').html('').hide()
      $(form).find('.sent-message').html(response.message).fadeIn(150)

      return true
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus, errorThrown)
    }
  })
}